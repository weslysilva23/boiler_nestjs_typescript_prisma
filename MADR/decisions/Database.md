---
status: { Pendente }
data: { dd-mm-aaaa }
reponsavel: {
    TechLead: Wesly Silva,
}
decisores: {
    Arquitetura:  Não Definida.
},

consultados: {
    PO:  Não Definida.,
    TechLead: Wesly Silva,
    GerenteProjetos:  Não Definida.,
    Squad: Não Definida.
},

informados: {
    Arquitetura:  Não Definida,
    PO:  Não Definida.,
    TechLead:  Não Definida.,
    Gesrente Projeto:  Não Definida.,
    Arquitetura:  Não Definida.,
    Squad: Não Definida.
}.
---

# Database

## Contexto

{ O postgres banco de dados escolhido segue o padrão determinado pela arquitetura de referência. }

## Motivadores de decisão

* Seguir a arquitetura de referencia 

## Opções Consideradas

* Nenhuma.


## Resultado da decisão

Decidido unilateralmente pela arquitetura de referencia.


### Consequencias

* Os dados deste projeto tem caracteristica dinamica podendo sofrer alteração em qualquer tempo o que torna
necessario um banco de dados não relacional mais indicado para esta soluçãoo, apesar do postgre trabalhar com coleção
nenhuma direção a respeito do seu uso foi abordada pela arquitetura de refencia.

## Validação

Não houve validação da solução


