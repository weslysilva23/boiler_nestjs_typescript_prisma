---
status: { Pendente }
data: { dd-mm-aaaa }
reponsavel: {
    TechLead: Wesly Silva,
}
decisores: {
    Arquitetura:  Não Definida.
},

consultados: {
    PO:  Não Definida.,
    TechLead: Wesly Silva,
    GerenteProjetos:  Não Definida.,
    Squad: Não Definida.
},

informados: {
    Arquitetura:  Não Definida,
    PO:  Não Definida.,
    TechLead:  Não Definida.,
    Gesrente Projeto:  Não Definida.,
    Arquitetura:  Não Definida.,
    Squad: Não Definida.
}.
---
# FrameWork NESTJS

## Contexto

{ O NESTJS framework para backend escolhido pela arquitetura para construção do BFF segue o padrão determinado pela arquitetura de referência. }

## Motivadores de decisão

* Seguir a arquitetura de referencia 

## Opções Consideradas

* Nenhuma.


## Resultado da decisão

Decidido em cominte durante a construção da arquitetura de referencia.


### Consequencias

* Nenhum padrão de projeto foi definido apenas o uso do framework
* nenhuma estrutura 

## Validação

- Não houve validação da solução


