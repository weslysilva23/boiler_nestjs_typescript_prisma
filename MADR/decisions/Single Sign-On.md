---
status: { Pendente }
data: { dd-mm-aaaa }
reponsavel: {
    TechLead: Wesly Silva,
}
decisores: {
    Arquitetura:  Não Definida.
},

consultados: {
    PO:  Não Definida.,
    TechLead: Wesly Silva,
    GerenteProjetos:  Não Definida.,
    Squad: Não Definida.
},

informados: {
    Arquitetura:  Não Definida,
    PO:  Não Definida.,
    TechLead:  Não Definida.,
    Gesrente Projeto:  Não Definida.,
    Arquitetura:  Não Definida.,
    Squad: Não Definida.
}.
---

# IDS Identity provider

## Contexto

{ Durante as fases de discovery do projeto foi identificado que o cliente precisa de uma autenticação
utilizando a propria plataforma de login ou IDS da corporação para que os usuarios não tivessem a necessidade
de realizar um cadastro manual na plataforma. }

## Motivadores de decisão

* Login Centralizado
* Reaproveitamento de contas

## Opcoes consideradas

* AD Microsoft

## Resultado da decisão

A ferramenta escolhida AD da Microsoft ja em operação em toda a corporação
incluindo suas filias o que facilitara o processo de login.

### Consequencias

* BOM, Unificou o login do cliente, tornando mais facil a centralização de autenticação e autorização.
* BOM, Integração com AD
* RUIM, não sera possivel cadastro de terceiros, caso haja a necessidade de cadastrar um conta com email que não seja mills.

## Validação

Este processo foi validado pela arquitetura em outros projetos da mills e trazido de forma unilaterl 