---
status: { Pendente }
data: { dd-mm-aaaa } #  Creation Date of document
reponsavel: {
    TechLead: Name lastname
}
decisores: {
    Arquitetura: Name lastname, Name lastname
},

consultados: {
    PO: Name lastname,
    TechLead: Name lastname,
    GerenteProjetos: Name lastname,
    Squad: Name lastname, Name lastname, Name lastname
},

informados: {
    Arquitetura: Name lastname, Name lastname,
    PO: Name lastname,
    TechLead: Name lastname,
    GerenteProjetos: Name lastname,
    Gerente: Name lastname,
    Gerente Arquitetura: Name lastname,
    Squad: Name lastname, Name lastname,
}.
---

# IDS Identity provider

## Contexto

{ Forneça contexto ao descrever algo, pois isso é essencial para tomar decisões informadas. }

## Motivadores de decisão

* Motivador 1
* Motivador 2

## Opcoes consideradas

* Opção 1
* Opção 2
* Opção 3


## Resultado da decisão

Força uma breve descrição da descisão tomada e porque.

### Consequencias

* BOM, Qual beneficio foi obtido com a descisão, coloque 1 por item.
* RUIM, Quais risco a descisão pode trazer, coloque 1 por item.

## Validação

De quqe mandeira esse processo foi validado para chegar a conclusão da opção considerada.

