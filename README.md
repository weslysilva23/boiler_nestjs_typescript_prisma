<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.


## Require

use node 20.10 LTS

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```


## Prisma

```bash

# Prima initial migration

# Format for correction relationship
$ npx prisma format

$ npx prisma migrate dev -n init
$ npx prisma migrate dev -n nome-da-nova-migration 

# Prisma Deploy
$ npx prisma migrate deploy

# Generate migration
$ npx prisma generate
$ npx prisma generate --force

# Prisma Studio for view database and CRUD operation.
$ npx prisma studio

# New migration without check schema.prisma
$ npx prisma migrate dev -n initial_data --create-only 

# Use for apply the last migration
$ npx prima migrate up


#  Restore state when your migrate is fail 
$  npx prisma migrate resolve --rolled-back "migrate_name"



```


## Nest

```bash

#Generate Resource Modules
$ nest g res modules/module_name 

#Generate Resource Module
$ nest g res module module_name 

#Remove Resource Module
$ neste remove module modules/module_name 

#Generate Service
nest g service prisma

```


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Wesly Silva](https://www.linkedin.com/in/weslysousa/)

## License

Nest is [MIT licensed](LICENSE).
