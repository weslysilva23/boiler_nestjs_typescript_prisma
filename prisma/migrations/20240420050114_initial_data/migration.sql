BEGIN;

INSERT INTO public.category(name) VALUES 
('Leves'),
('Pesados');

INSERT INTO public.family(name) VALUES 
('BB'),
('BD'),
('BE'),
('BH'),
('EP'),
('TD'),
('TE');

INSERT INTO public.sub_family(name) VALUES
('BB105'),
('BB120'),
('BB125A'),
('BB125T'),
('BB135A'),
('BB135T'),
('BB150T'),
('BB180'),
('BD034'),
('BD045'),
('BD052'),
('BD060'),
('BD065'),
('BD080'),
('BD085'),
('BE030'),
('BE034'),
('BE042'),
('BE045'),
('BE060'),
('BH045'),
('BH060'),
('BH138T'),
('BH78T'),
('EP006'),
('EP010'),
('EP013'),
('EP020'),
('EP025'),
('EP026'),
('EP030'),
('EPM030'),
('EPM036'),
('EPM039'),
('EPM040'),
('EPM041'),
('TD026'),
('TD032'),
('TD033'),
('TD041'),
('TD043'),
('TD050'),
('TD053'),
('TE013'),
('TE019'),
('TE020'),
('TE026'),
('TE032'),
('TE033'),
('TE040N'),
('TE040R'),
('TE046');

INSERT INTO public.fabricator(name) VALUES
('GENIE'),
('JLG'),
('HAULOTTE'),
('SKYJACK'),
('MANITOU'),
('DINGLI'),
('HYDROLIFT');

COMMIT;