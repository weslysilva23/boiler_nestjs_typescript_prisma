/*
  Warnings:

  - Added the required column `code` to the `branch` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "branch" ADD COLUMN     "code" INTEGER NOT NULL;
