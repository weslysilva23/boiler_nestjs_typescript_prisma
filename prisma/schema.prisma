generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model User {
  id        Int      @id @default(autoincrement())
  email     String   @unique
  name      String
  admin     Boolean  @default(false)
  role      String
  branch    Branch?  @relation(fields: [branchId], references: [id])
  branchId  Int
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  @@map("user")
}

model Branch {
  id          Int          @id @default(autoincrement())
  name        String
  sigla       String
  location    String
  code        Int
  cnpj        String?
  Users       User[]
  Equipaments Equipament[]
  createdAt   DateTime     @default(now())
  updatedAt   DateTime     @default(now())

  @@map("branch")
}

enum Importacao {
  PC38
  Manual
}

enum Status {
  Ativo
  Inativo
}

model Equipament {
  id            Int        @id @default(autoincrement())
  tag           String
  serial_number String
  sales_plan    String
  name          String
  status        Status     @default(Ativo)
  lastBookDate  String
  lastBookurl   String
  description   String
  price         String
  importacao    Importacao @default(Manual)
  createdAt     DateTime   @default(now())
  updatedAt     DateTime   @default(now())

  branch   Branch? @relation(fields: [branchId], references: [id])
  branchId Int

  model              EquipamentsModel @relation(fields: [equipamentsModelId], references: [id])
  equipamentsModelId Int

  Catalog            Catalog[]

  @@map("equipament")
}

model Category {
  id               Int                @id @default(autoincrement())
  name         String             @unique
  EquipamentsModel EquipamentsModel[]
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  @@map("category")
}

model Family {
  id               Int                @id @default(autoincrement())
  name           String             @unique
  EquipamentsModel EquipamentsModel[]
  createdAt DateTime    @default(now())
  updatedAt DateTime    @default(now())
  Checklist Checklist[]

  @@map("family")
}

model Sub_Family {
  id               Int                @id @default(autoincrement())
  name        String             @unique
  EquipamentsModel EquipamentsModel[]
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  @@map("sub_family")
}

model Fabricator {
  id               Int                @id @default(autoincrement())
  name       String             @unique
  EquipamentsModel EquipamentsModel[]
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  @@map("fabricator")
}

model EquipamentsModel {
  id    Int    @id @default(autoincrement())
  name String

  category   Category? @relation(fields: [categoryId], references: [id])
  categoryId Int

  family   Family? @relation(fields: [familyId], references: [id])
  familyId Int

  subFamily    Sub_Family? @relation(fields: [sub_FamilyId], references: [id])
  sub_FamilyId Int?

  fabricator   Fabricator? @relation(fields: [fabricatorId], references: [id])
  fabricatorId Int

  createdAt  DateTime     @default(now())
  updatedAt  DateTime     @default(now())
  Equipament Equipament[]
  Checklist  Checklist[]
  Book       Book[]

  @@map("equipaments_model")
}

model Checklist {
  id        Int      @id @default(autoincrement())
  name      String
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  equipamentModel    EquipamentsModel? @relation(fields: [equipamentsModelId], references: [id])
  equipamentsModelId Int?

  family   Family? @relation(fields: [familyId], references: [id])
  familyId Int

  ChecklistToQuestions ChecklistToQuestions[]

  @@map("checklist")
}

model Questions {
  id                   Int                    @id @default(autoincrement())
  question             String
  description          String?
  createdAt            DateTime               @default(now())
  updatedAt            DateTime               @default(now())
  ChecklistToQuestions ChecklistToQuestions[]

  @@map("question")
}

model ChecklistToQuestions {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  checklist   Checklist? @relation(fields: [checklistId], references: [id])
  checklistId Int

  question         Questions           @relation(fields: [questionId], references: [id])
  questionId       Int
  AnswersChecklist AnswersChecklist[]

  @@map("checklistToQuestions")

}

model Book {
  id        Int      @id @default(autoincrement())
  name      String
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  equipamentModel    EquipamentsModel @relation(fields: [equipamentsModelId], references: [id])
  equipamentsModelId Int

  BookToPhotos BookToPhotos[]
  @@map("book")
}

model Photos {
  id           Int            @id @default(autoincrement())
  name         String
  description  String
  example_Url  String
  createdAt    DateTime       @default(now())
  updatedAt    DateTime       @default(now())
  BookToPhotos BookToPhotos[]

  @@map("photos")
}

model BookToPhotos {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())
  photo     Photos   @relation(fields: [photosId], references: [id])
  book      Book     @relation(fields: [bookId], references: [id])

  photosId    Int
  bookId      Int
  TakedPhotos TakedPhotos[]

  @@map("bookToPhotos")
}

model Catalog {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  updatedAt DateTime @default(now())

  equipament   Equipament @relation(fields: [equipamentId], references: [id])
  equipamentId Int

  AnswersChecklist AnswersChecklist[]
  TakedPhotos      TakedPhotos[]

  @@map("catalog")
}

model AnswersChecklist {
  id                     Int                  @id @default(autoincrement())
  checklist              ChecklistToQuestions @relation(fields: [checklistToQuestionsId], references: [id])
  answer                 String
  catalog                Catalog              @relation(fields: [catalogId], references: [id])
  createdAt              DateTime             @default(now())
  updatedAt              DateTime             @default(now())
  checklistToQuestionsId Int
  catalogId              Int

  @@map("answersChecklist")
}

model TakedPhotos {
  id             Int          @id @default(autoincrement())
  createdAt      DateTime     @default(now())
  updatedAt      DateTime     @default(now())
  catalog        Catalog      @relation(fields: [catalogId], references: [id])
  catalogId      Int
  photoTakedUrl  String
  bookToPhoto    BookToPhotos @relation(fields: [bookToPhotosId], references: [id])
  bookToPhotosId Int

  @@map("takedPhotos")
}
