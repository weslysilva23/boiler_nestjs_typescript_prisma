import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { HealthModule } from './modules/health/health.module';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './modules/user/user.module';
import { EquipamentModule } from './modules/equipament/equipament.module';
import { AuthMiddleware } from '@common/middleware';
import { BranchModule } from './modules/branch/branch.module';
import { FamilyModule } from './modules/family/family.module';
import { SubfamilyModule } from './modules/subfamily/subfamily.module';
import { CategoryModule } from './modules/category/category.module';
import { EquipamentsModelModule } from './modules/equipaments-model/equipaments-model.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    HealthModule,
    UserModule,
    EquipamentModule,
    BranchModule,
    FamilyModule,
    SubfamilyModule,
    CategoryModule,
    EquipamentsModelModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).exclude('/health').forRoutes('*');
  }
}
