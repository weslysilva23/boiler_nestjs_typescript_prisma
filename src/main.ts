import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import { HttpsExeceptionFilter } from './shared/common/filters';
import {
  DataBaseInterceptor,
  HttpInterceptor,
} from './shared/common/interceptor';
import {
  DocumentBuilder,
  SwaggerDocumentOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { LoggerInterceptor } from '@common/interceptor/Logger.interceptor';
import { PrismaInterceptor } from '@common/interceptor/prisma.interceptor';
import { Package } from '@common/utils';

async function bootstrap() {
  const pack = new Package();
  const version = pack.getVersion();

  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    }),
  );

  app.useGlobalFilters(new HttpsExeceptionFilter());
  app.useGlobalInterceptors(new LoggerInterceptor());
  app.useGlobalInterceptors(new HttpInterceptor());
  app.useGlobalInterceptors(new PrismaInterceptor());
  app.useGlobalInterceptors(new DataBaseInterceptor());

  const swaggerDocumentBuilder = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Api Documentation')
    .setDescription('OpenApi documentation')
    .setVersion(version)
    .addServer('http://localhost:3000', 'Local')
    .addServer(process.env.DOCUMENTATION_SERVER_HML, 'Homologation')
    .addServer(process.env.DOCUMENTATION_SERVER_PRD, 'Production')
    .build();

  const swaggerDocumentOptions: SwaggerDocumentOptions = {
    operationIdFactory: (_controllerKey: string, methodKey: string) =>
      methodKey,
  };

  const swaggerDocument = SwaggerModule.createDocument(
    app,
    swaggerDocumentBuilder,
    swaggerDocumentOptions,
  );

  SwaggerModule.setup('docs', app, swaggerDocument);

  await app.listen(process.env.APPLICATION_PORT, () => {
    console.log(
      `Server Version: ${version} is running in`,
      process.env.APPLICATION_PORT,
    );
  });
}

bootstrap();
