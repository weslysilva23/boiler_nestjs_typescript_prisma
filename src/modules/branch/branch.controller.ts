import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete
} from '@nestjs/common';
import { BranchService } from './branch.service';
import { CreateBranchDto } from './dto/Req/create-branch.dto';
import { UpdateBranchDto } from './dto/Req/update-branch.dto';
import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { BranchResDto } from './dto/Res/branch-res.dto';

@ApiBearerAuth()
@ApiTags('Branch')
@Controller('branch')
export class BranchController {
  constructor(private readonly branchService: BranchService) {}

  @ApiResponse({ type: BranchResDto })
  @ApiBody({ type: CreateBranchDto })
  @Post()
   create(@Body() createBranchDto: CreateBranchDto) {
    return this.branchService.create(createBranchDto);
  }

  @ApiResponse({ type: BranchResDto })
  @Get()
   findAll() {
    return this.branchService.findAll();
  }

  @ApiResponse({ type: BranchResDto })
  @Get(':id')
   findOne(@Param('id') id: string) {
    return this.branchService.findOne(+id);
  }

  @ApiResponse({ type: BranchResDto })
  @Delete(':id')
   remove(@Param('id') id: string) {
    return this.branchService.remove(+id);
  }

  @ApiResponse({ type: BranchResDto })
  @Patch(':id')
   update(
    @Param('id') id: string,
    @Body() updateBranchDto: UpdateBranchDto,
  ) {
    return this.branchService.update(+id, updateBranchDto);
  }
}