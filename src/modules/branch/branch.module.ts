import { Module } from '@nestjs/common';
import { BranchService } from './branch.service';
import { BranchController } from './branch.controller';
import { PrismaService } from '@services/prisma/prisma.service';
import { BranchRepository } from './repositories/branch.repository';

@Module({
  controllers: [BranchController],
  providers: [BranchService, PrismaService, BranchRepository],
})
export class BranchModule {}
