import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/Req/create-branch.dto';
import { UpdateBranchDto } from './dto/Req/update-branch.dto';
import { BranchRepository } from './repositories/branch.repository';
import { NotFoundError } from '@common/types';

@Injectable()
export class BranchService {
  constructor(private readonly branchRepo: BranchRepository) {}

  async create(createBranchDto: CreateBranchDto) {
    try {
      const branchCreated = await this.branchRepo.create(createBranchDto);
      return branchCreated;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    const branchs = await this.branchRepo.findAll();
    return branchs;
  }

  async findOne(id: number) {
    let branch = await this.branchRepo.findOne(id);

    if (branch) {
      return branch;
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  async remove(id: number) {
    return await this.branchRepo.remove(id);
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    return await this.branchRepo.update(id, updateBranchDto);
  }
}
