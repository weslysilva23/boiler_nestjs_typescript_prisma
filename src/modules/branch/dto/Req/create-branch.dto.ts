import { ApiProperty } from '@nestjs/swagger';
import { Branch } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsDate, IsNumber, IsString } from 'class-validator';

export class CreateBranchDto implements Omit<Branch, 'id' | 'createdAt'> {
  @ApiProperty()
  @IsString()
  cnpj: string;
  @ApiProperty()
  @IsNumber()
  code: number;
  @ApiProperty()
  @IsString()
  location: string;
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  sigla: string;
  @Transform(param => new Date())
  @IsDate()
  updatedAt: Date = new Date();
}
