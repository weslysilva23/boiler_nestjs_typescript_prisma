import { IResponse } from '@common/interfaces/http';
import { Meta } from '@shared/common/classes';
import {
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { BranchEntity } from '@modules/branch/entities/Branch.entity';

export class BranchResDto implements IResponse<BranchEntity> {
  @ApiProperty()
  @ValidateNested({ each: true })
  @Type(() => BranchEntity)
  data: BranchEntity;

  @ApiProperty({
    required: false,
  })
  @ValidateNested({ each: true })
  @Type(() => Meta)
  @IsOptional()
  meta?: Meta;

  @ApiProperty()
  @IsString({ message: 'sucess' })
  status: string;

  @ApiProperty()
  @IsNumber()
  statusCode: number;

  @ApiProperty({
    required: false,
    description: 'O sumaridado e uma propriede que pode ou não estar presente.',
  })
  @IsOptional()
  @IsDefined({ message: 'A propriedade deve ser definida' })
  summary?: any;
}
