import { CreateBranchDto } from './Req/create-branch.dto';
import { UpdateBranchDto } from './Req/update-branch.dto';

export { CreateBranchDto, UpdateBranchDto };
