import { ApiProperty } from '@nestjs/swagger';
import { Branch } from '@prisma/client';

export class BranchEntity implements Branch {
  @ApiProperty({ type: 'string', description: 'CNPJ of the branch' })
  cnpj: string;
  @ApiProperty({ type: 'number', description: 'code of the branch' })
  code: number;
  @ApiProperty({ type: 'string', description: 'Date the branch was created' })
  createdAt: Date;
  @ApiProperty({ type: 'number', description: 'ID of the branch' })
  id: number;
  @ApiProperty({ type: 'string', description: 'Location of the branch' })
  location: string;
  @ApiProperty({ type: 'string', description: 'Name of the branch' })
  name: string;
  @ApiProperty({ type: 'string', description: 'sigla of the branch' })
  sigla: string;
  @ApiProperty({
    type: 'string',
    description: 'Date the branch was last updated',
  })
  updatedAt: Date;
}
