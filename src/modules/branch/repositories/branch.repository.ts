import { Injectable } from '@nestjs/common';
import { PrismaService } from '@services/prisma/prisma.service';
import { CreateBranchDto, UpdateBranchDto } from '../dto';
import { BranchEntity } from '../entities/Branch.entity';
import { NotFoundError } from '@common/types/http.types';

@Injectable()
export class BranchRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(createBranchDto: CreateBranchDto): Promise<BranchEntity> {
    return this.prisma.branch.create({
      data: createBranchDto,
    });
  }

  async findAll(): Promise<BranchEntity[]> {
    try {
      return this.prisma.branch.findMany();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<BranchEntity> {
    //TODO Caso Brancho nao seja encontrado retornar um Not Found - throw new NotFoundError

    let Branch = await this.prisma.branch.findUnique({
      where: {
        id,
      },
    });

    if (Branch) {
      return Branch;
    } else {
      throw new NotFoundError('Branch Not Found');
    }
  }

  async remove(id: number): Promise<BranchEntity> {
    let deleted = await this.prisma.branch.delete({
      where: {
        id,
      },
    });

    return deleted;
  }

  async update(
    id: number,
    updateBranchDto: UpdateBranchDto,
  ): Promise<BranchEntity> {
    let updated = await this.prisma.branch.update({
      data: updateBranchDto,
      where: {
        id,
      },
    });

    return updated;
  }
}
