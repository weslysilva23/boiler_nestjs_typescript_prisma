import { Injectable } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryRepository } from './repositories/category.repository';
import { NotFoundError } from '@common/types';

@Injectable()
export class CategoryService {
  constructor(private readonly categoryRepo: CategoryRepository) {}

  async create(createCategoryDto: CreateCategoryDto) {
    try {
      const categoryCreated = await this.categoryRepo.create(createCategoryDto);
      return categoryCreated;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const categories = await this.categoryRepo.findAll();
      return categories;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number) {
    let category = await this.categoryRepo.findOne(id);

    if (category) {
      return category;
    } else {
      throw new NotFoundError('Category Not Found');
    }
  }

  async remove(id: number) {
    return await this.categoryRepo.remove(id);
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    try {
      return await this.categoryRepo.update(id, updateCategoryDto);
    } catch (error) {
      throw error;
    }
  }
}
