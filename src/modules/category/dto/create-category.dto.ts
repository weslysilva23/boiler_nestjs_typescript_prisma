import { ApiProperty } from '@nestjs/swagger';
import { Category } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsDate, IsString } from 'class-validator';

export class CreateCategoryDto implements Omit<Category, 'id' | 'createdAt'> {
  @ApiProperty()
  @IsString()
  name: string;

  @Transform(param => new Date())
  @IsDate()
  updatedAt: Date = new Date();
}
