import { ApiProperty } from '@nestjs/swagger';
import { Category } from '@prisma/client';

export class CategoryEntity implements Category {
  @ApiProperty({ type: 'string', description: 'Date the category was created' })
  createdAt: Date;
  @ApiProperty({ type: 'number', description: 'ID of the Category' })
  id: number;

  @ApiProperty({ type: 'string', description: 'Name of the Category' })
  name: string;
  @ApiProperty({
    type: 'string',
    description: 'Date the category was last updated',
  })
  updatedAt: Date;
}
