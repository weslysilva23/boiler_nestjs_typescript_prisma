import { Injectable } from '@nestjs/common';
import { PrismaService } from '@services/prisma/prisma.service';
import { CreateCategoryDto, UpdateCategoryDto } from '../dto';
import { CategoryEntity } from '../entities/Category.entity';
import { NotFoundError } from '@common/types/http.types';

@Injectable()
export class CategoryRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(createCategoryDto: CreateCategoryDto): Promise<CategoryEntity> {
    return this.prisma.category.create({
      data: createCategoryDto,
    });
  }

  async findAll(): Promise<CategoryEntity[]> {
    try {
      return this.prisma.category.findMany();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<CategoryEntity> {
    //TODO Caso Categoryo nao seja encontrado retornar um Not Found - throw new NotFoundError

    let Category = await this.prisma.category.findUnique({
      where: {
        id,
      },
    });

    if (Category) {
      return Category;
    } else {
      throw new NotFoundError('Category Not Found');
    }
  }

  async remove(id: number): Promise<CategoryEntity> {
    let deleted = await this.prisma.category.delete({
      where: {
        id,
      },
    });

    return deleted;
  }

  async update(
    id: number,
    updateCategoryDto: UpdateCategoryDto,
  ): Promise<CategoryEntity> {
    let updated = await this.prisma.category.update({
      data: updateCategoryDto,
      where: {
        id,
      },
    });

    return updated;
  }
}
