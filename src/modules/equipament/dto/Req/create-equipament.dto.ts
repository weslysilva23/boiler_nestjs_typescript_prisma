import { ApiProperty } from '@nestjs/swagger';
import { Equipament } from '@prisma/client';
import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateEquipamentDto
  implements
    Omit<
      Equipament,
      'id' | 'createdAt' | 'updatedAt' | 'importacao' | 'status'
    >
{
  @ApiProperty({ type: 'number', description: 'ID of the branchModel' })
  @IsNumber({}, { message: 'O branchId deve ser uma numero' })
  branchId: number;
  @ApiProperty()
  @IsString({ message: 'O atributo description deve ser uma string' })
  description: string;
  @ApiProperty({ type: 'number', description: 'ID of the equipamentsModel' })
  @IsNumber({}, { message: 'O equipamentsModelId deve ser um número' })
  equipamentsModelId: number;
  @ApiProperty({ type: 'string', description: 'Date of the last book' })
  @IsDate()
  @IsOptional()
  lastBookDate: string;
  @ApiProperty({ type: 'string', description: 'url of the last book' })
  @IsString({ message: 'O atributo lastBookurl deve ser uma string' })
  @IsOptional()
  lastBookurl: string;
  @ApiProperty({ type: 'string', description: 'name of the equipament' })
  @IsString({ message: 'O atributo name deve ser uma string' })
  name: string;
  @ApiProperty()
  @IsString({ message: 'O atributo price deve ser uma string' })
  price: string;
  @ApiProperty({ type: 'string', description: 'sales channel' })
  @IsString({ message: 'O atributo sales_plan deve ser uma string' })
  sales_plan: string;
  @ApiProperty({
    type: 'string',
    description: 'serial number of the equipament',
  })
  @IsString({ message: 'O atributo serial_number deve ser uma string' })
  serial_number: string;
  @ApiProperty({ type: 'string', description: 'tag of the equipament' })
  @IsString({ message: 'O atributo tag deve ser uma string' })
  tag: string;
}
