import { EOrder } from '@common/enum';
import { IPaginationQuery } from '@common/interfaces/http';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsNumber,
  IsInt,
  Max,
  IsOptional,
  IsEnum,
  IsString,
} from 'class-validator';

export default class EquipamentPaginationQuery implements IPaginationQuery {
  @ApiProperty({
    required: false,
    enum: EOrder,
    example: 'ASC',
    description:
      'Respective Key:   ' +
      Object.keys(EOrder)
        .filter(key => !/\d/.test(key))
        .toString(),
  })
  @IsOptional()
  @IsEnum(EOrder)
  order: EOrder;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  orderBy: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @Transform(param => Number.parseInt(param.value))
  @IsInt()
  @IsNumber()
  pageNumber: number;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  @IsInt()
  @Max(100)
  pageSize: number;
}
