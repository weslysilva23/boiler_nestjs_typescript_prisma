import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsNotEmpty } from 'class-validator';
import { Express } from 'express';

export class UploadFileDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  @IsDefined()
  @IsNotEmpty()
  @Type(() => Object)
  private file: Express.Multer.File;
}
