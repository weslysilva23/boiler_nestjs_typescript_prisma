import { IResponse } from '@common/interfaces/http';
import { Meta } from '@shared/common/classes';
import {
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { EquipamentEntity } from '@modules/equipament/entities/equipament.entity';

export class EquipamentResDto implements IResponse<EquipamentEntity> {
  @ApiProperty()
  @ValidateNested({ each: true })
  @Type(() => EquipamentEntity)
  data: EquipamentEntity;
  @ApiProperty()
  @ValidateNested({ each: true })
  @Type(() => Meta)
  @IsOptional()
  meta?: Meta;
  @ApiProperty()
  @IsString({ message: 'sucess' })
  status: string;
  @ApiProperty()
  @IsNumber()
  statusCode: number;
  @ApiProperty({
    description: 'O sumaridado e uma propriede que pode ou não estar presente.',
  })
  @IsOptional()
  @IsDefined({ message: 'A propriedade deve ser definida' })
  summary?: any;
}
