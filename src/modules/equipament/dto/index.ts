import { CreateEquipamentDto } from './Req/create-equipament.dto';
import { UpdateEquipamentDto } from './Req/update-equipament.dto';
import { EquipamentResDto } from './Res/response-equipament.dto';

export { CreateEquipamentDto, UpdateEquipamentDto, EquipamentResDto };
