import {
  IsDate,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Equipament, Importacao, Status } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class EquipamentEntity implements Equipament {
  @ApiProperty({ type: 'number', description: 'ID of the branchModel' })
  @IsNumber({}, { message: 'O branchId deve ser uma numero' })
  branchId: number;

  @ApiProperty()
  @IsString({ message: 'O atributo description deve ser uma string' })
  description: string;

  @ApiProperty({ type: 'number', description: 'ID of the equipamentsModel' })
  @IsNumber({}, { message: 'O equipamentsModelId deve ser um número' })
  equipamentsModelId: number;

  @ApiProperty({
    type: 'number',
    description: 'ID auto incrementado no banco de dados',
  })
  @IsNumber(
    {},
    { message: 'This Number is auto generate and auto increment in database' },
  )
  id: number;

  @ApiProperty({
    type: 'enum',
    description: 'ID of the equipamentsModel',
    enumName: 'Importacao',
    enum: Importacao,
    example: Importacao.PC38,
  })
  @IsEnum(Importacao, { message: 'A importação não é válida' })
  importacao: Importacao;

  @ApiProperty({ type: 'string', description: 'Date of the last book' })
  @IsDate()
  @IsOptional()
  lastBookDate: string;

  @ApiProperty({ type: 'string', description: 'url of the last book' })
  @IsString({ message: 'O atributo lastBookurl deve ser uma string' })
  @IsOptional()
  lastBookurl: string;

  @ApiProperty({ type: 'string', description: 'name of the equipament' })
  @IsString({ message: 'O atributo name deve ser uma string' })
  name: string;

  @ApiProperty()
  @IsString({ message: 'O atributo price deve ser uma string' })
  price: string;

  @ApiProperty({ type: 'string', description: 'sales channel' })
  @IsString({ message: 'O atributo sales_plan deve ser uma string' })
  sales_plan: string;

  @ApiProperty({
    type: 'string',
    description: 'serial number of the equipament',
  })
  @IsString({ message: 'O atributo serial_number deve ser uma string' })
  serial_number: string;

  @ApiProperty({ enum: Status, enumName: 'Status', example: Status.Ativo })
  @IsEnum(Status, { message: 'O status fornecido não é válido' })
  status: Status;

  @ApiProperty({ type: 'string', description: 'tag of the equipament' })
  @IsString({ message: 'O atributo tag deve ser uma string' })
  tag: string;

  @ApiProperty({ type: 'string', description: 'date is genarated in database' })
  @IsDate({ message: 'O atributo CreatedAt não esta presente' })
  createdAt: Date;

  @ApiProperty({ type: 'string', description: 'date is genarated in database' })
  @IsDate({ message: 'O atributo UpdatedAt não esta presente' })
  updatedAt: Date;
}
