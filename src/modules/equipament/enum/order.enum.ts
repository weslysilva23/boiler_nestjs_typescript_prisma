export enum EOrder {
  Ascending = 'ASC',
  Descending = 'DESC',
}
