import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UseGuards,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiBody, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { EquipamentService } from './equipament.service';
import { CreateEquipamentDto } from './dto/Req/create-equipament.dto';
import { UpdateEquipamentDto } from './dto/Req/update-equipament.dto';
import { EquipamentResDto } from './dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { CurrentUser } from '@common/decorator';
import { RoleGuard } from '@shared/middlewares/guard/role.guard';
import EquipamentPaginationQuery from './dto/Req/equipament-pagination-query.dto';

@ApiBearerAuth()
@ApiTags('Equipamentos')
@Controller('equipament')
export class EquipamentController {
  constructor(private readonly equipamentService: EquipamentService) {}

  @ApiResponse({ type: EquipamentResDto })
  @Get(':id')
  private findOne(@Param('id') id: string) {
    return this.equipamentService.findOne(+id);
  }

  @ApiResponse({ type: EquipamentResDto })
  @Delete(':id')
  private remove(@Param('id') id: string) {
    return this.equipamentService.remove(+id);
  }

  @ApiResponse({ type: EquipamentResDto })
  @Patch(':id')
  private update(
    @Param('id') id: string,
    @Body() updateEquipamentDto: UpdateEquipamentDto,
  ) {
    return this.equipamentService.update(+id, updateEquipamentDto);
  }

  @ApiResponse({ type: EquipamentResDto })
  @ApiBody({ type: CreateEquipamentDto })
  @Post()
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  async create(
    @Body() createEquipamentDto: CreateEquipamentDto,
    @CurrentUser() user,
  ): Promise<EquipamentResDto> {
    console.log(user);

    const equipament = await this.equipamentService.create(createEquipamentDto);

    return {
      status: 'created',
      statusCode: 201,
      data: equipament,
    };
  }

  @ApiResponse({ type: EquipamentResDto })
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  @Get()
  async findAll(
    @CurrentUser() user,
    @Query() params: EquipamentPaginationQuery,
  ) {
    console.log(user);
    try {
      return this.equipamentService.findAll();
    } catch (error) {
      console.log(error);
    }
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    await this.equipamentService.processAndPersistFile(file);
  }
}
