import { Module } from '@nestjs/common';
import { EquipamentService } from './equipament.service';
import { EquipamentController } from './equipament.controller';
import { PrismaService } from '@services/prisma/prisma.service';
import { EquipamentRepository } from './repositories/equipament.repository';

@Module({
  controllers: [EquipamentController],
  providers: [EquipamentService, PrismaService, EquipamentRepository],
})
export class EquipamentModule {}
