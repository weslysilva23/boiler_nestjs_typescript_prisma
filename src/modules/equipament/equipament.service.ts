import { Injectable } from '@nestjs/common';
import { CreateEquipamentDto } from './dto/Req/create-equipament.dto';
import { UpdateEquipamentDto } from './dto/Req/update-equipament.dto';
import * as fs from 'fs';
import * as csvParser from 'csv-parser';
import * as xlsParser from 'xlsx';
import { NotFoundError } from '@common/types/http.types';
import { EquipamentRepository } from './repositories/equipament.repository';
import { Equipament } from '@prisma/client';

@Injectable()
export class EquipamentService {
  constructor(private readonly equipamentoRepo: EquipamentRepository) {}

  async create(createEquipamentDto: CreateEquipamentDto): Promise<Equipament> {
    //TODO verificar se o equipamento ja existe se existe apenas atualizar as informações.
    //TODO Senao criar

    try {
      const equipament = await this.equipamentoRepo.create(createEquipamentDto);
      return equipament;
    } catch (error) {
      console.error('Erro ao tentar adicionar um novo equipamento');
      console.error(createEquipamentDto);

      throw error;
    }
  }

  async findAll(): Promise<Equipament[]> {
    try {
      return this.equipamentoRepo.findAll();
    } catch (error) {
      console.log(`Error to try get all equipaments`, error);
      throw error;
    }
  }

  async findOne(id: number) {
    //TODO Caso equipamento nao seja encontrado retornar um Not Found - throw new NotFoundError

    let equipament = await this.equipamentoRepo.findOne(id);

    if (equipament) {
      return equipament;
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  async update(id: number, updateEquipamentDto: UpdateEquipamentDto) {
    return `This action updates a #${id} equipament`;
  }

  async remove(id: number) {
    return `This action removes a #${id} equipament`;
  }

  // PARA PERSISITIR:
  //TODO Validar todos os equipamentos antes de começar o processo de persistencia.
  //TODO verificar se o equipamento ja existe se existe apenas atualizar as informações.
  //TODO Se não existir Criar o equipamento com status pendente.

  //TODO Possivelmente no futuro sera necessario exportar uma planilha de exemplo para que o
  // cliente nao coloque lixo durante o upload, isso requer que os dados cruzao com outras tabelas devem ser validados

  async processAndPersistFile(file: Express.Multer.File): Promise<void> {
    if (file.mimetype === 'text/csv') {
      const data = [];
      fs.createReadStream(file.path)
        .pipe(csvParser())
        .on('data', row => {
          data.push(row);
        })
        .on('end', () => {
          //TODO Processar os dados CSV e persistir

          console.log(data);

          // fs.unlink(file.path, (err) => {
          //   if (err) console.error(err);
          // });
        });
    } else if (file.mimetype === 'application/vnd.ms-excel') {
      const workbook = xlsParser.read(file.buffer, { type: 'buffer' });
      const worksheet = workbook.Sheets[workbook.SheetNames[0]];
      const data = xlsParser.utils.sheet_to_json(worksheet);

      //TODO Processar os dados XLS e persistir
      console.log(data);
    } else {
      throw new Error('Formato de arquivo inválido');
    }
  }
}
