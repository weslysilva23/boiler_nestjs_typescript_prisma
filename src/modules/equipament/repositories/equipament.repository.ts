import { Injectable } from '@nestjs/common';
import { PrismaService } from '@services/prisma/prisma.service';
import { CreateEquipamentDto, UpdateEquipamentDto } from '../dto';
import { EquipamentEntity } from '../entities/equipament.entity';
import { NotFoundError } from '@common/types/http.types';

@Injectable()
export class EquipamentRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(
    createEquipamentDto: CreateEquipamentDto,
  ): Promise<EquipamentEntity> {
    return this.prisma.equipament.create({
      data: createEquipamentDto,
    });
  }

  async findAll(): Promise<EquipamentEntity[]> {
    try {
      return this.prisma.equipament.findMany({
        orderBy: {
          model: {
            name: 'asc',
          },
        },
        include: {
          model: true,
        },
      });
    } catch (error) {
      console.log(`Error to try get all equipaments`, error);
      throw error;
    }
  }

  async findOne(id: number): Promise<EquipamentEntity> {
    //TODO Caso equipamento nao seja encontrado retornar um Not Found - throw new NotFoundError

    let equipament = await this.prisma.equipament.findUnique({
      where: {
        id,
      },
    });

    if (equipament) {
      return equipament;
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  async remove(id: number): Promise<EquipamentEntity> {
    let deleted = await this.prisma.equipament.delete({
      where: {
        id,
      },
    });

    return deleted;
  }

  async update(
    id: number,
    updateEquipamentDto: UpdateEquipamentDto,
  ): Promise<EquipamentEntity> {
    let updated = await this.prisma.equipament.update({
      data: updateEquipamentDto,
      where: {
        id,
      },
    });

    return updated;
  }
}
