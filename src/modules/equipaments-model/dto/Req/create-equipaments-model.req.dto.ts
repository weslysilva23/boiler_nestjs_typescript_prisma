import { ApiProperty } from '@nestjs/swagger';
import { EquipamentsModel } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsString, IsNumber, IsDate } from 'class-validator';

export class CreateEquipamentsModelDto
  implements Omit<EquipamentsModel, 'id' | 'createdAt'>
{
  @ApiProperty({ type: 'string', description: 'name of the equipamentModel' })
  @IsString({ message: 'O atributo name deve ser uma string' })
  name: string;

  @ApiProperty({ type: 'number', description: 'ID of the category' })
  @IsNumber({}, { message: 'O categoryId deve ser um número' })
  categoryId: number;

  @ApiProperty({ type: 'number', description: 'ID of the family' })
  @IsNumber({}, { message: 'O familyId deve ser um número' })
  familyId: number;

  @ApiProperty({ type: 'number', description: 'ID of the subfamily' })
  @IsNumber({}, { message: 'O subfamilyId deve ser um número' })
  sub_FamilyId: number;

  @ApiProperty({ type: 'number', description: 'ID of the fabricator' })
  @IsNumber({}, { message: 'O fabricatorId deve ser um número' })
  fabricatorId: number;

  @ApiProperty({
    type: 'string',
    description: 'Date the Family was last updated',
  })
  @Transform(param => new Date())
  @IsDate()
  updatedAt: Date = new Date();
}
