import { IPaginationQuery } from '@common/interfaces/http';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsOptional, IsBoolean } from 'class-validator';

export default class EquipamentsModelIncludeDataQuery
  implements IPaginationQuery
{
  @ApiProperty({
    required: false,
    example: false,
    description:
      'Include Data: Determines whether related data should be included in the query result. When set to true, additional information related to each equipment model, such as its family, category, fabricator, and sub-family, will be included in the response. This option can be used to fetch a more comprehensive dataset. By default, it is set to false, resulting in only the basic information of each equipment model being returned.',
  })
  @Transform(({ value }) => (value === 'false' ? false : true))
  @IsOptional()
  @IsBoolean()
  includeData: boolean;
}
