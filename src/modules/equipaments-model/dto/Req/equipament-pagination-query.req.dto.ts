import { EOrder } from '@common/enum';
import { IPaginationQuery } from '@common/interfaces/http';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsNumber,
  IsInt,
  Max,
  IsOptional,
  IsEnum,
  IsString,
  IsBoolean,
} from 'class-validator';

export default class EquipamentsModelPaginationQuery
  implements IPaginationQuery
{
  @ApiProperty({
    required: false,
    maximum: 100,
    minimum: 10,
    description: 'pageSize minimun 10 and maximum 100 elements per page',
  })
  @IsOptional()
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  @IsInt()
  @Max(100)
  pageSize: number;

  @ApiProperty({
    required: false,
    minimum: 1,
  })
  @IsOptional()
  @Transform(param => Number.parseInt(param.value))
  @IsInt()
  @IsNumber()
  pageNumber: number;

  @ApiProperty({
    required: false,
    enum: EOrder,
    example: 'ASC',
    description:
      'Respective Key:   ' +
      Object.keys(EOrder)
        .filter(key => !/\d/.test(key))
        .toString(),
  })
  @IsOptional()
  @IsEnum(EOrder)
  order: EOrder;

  @ApiProperty({
    required: false,
    example: 'name',
  })
  @IsOptional()
  @IsString()
  orderBy: string;

  @ApiProperty({
    required: false,
    example: false,
    description:
      'Include Data: Determines whether related data should be included in the query result. When set to true, additional information related to each equipment model, such as its family, category, fabricator, and sub-family, will be included in the response. This option can be used to fetch a more comprehensive dataset. By default, it is set to false, resulting in only the basic information of each equipment model being returned.',
  })
  @Transform(({ value }) => (value === 'false' ? false : true))
  @IsOptional()
  @IsBoolean()
  includeData: boolean;
}
