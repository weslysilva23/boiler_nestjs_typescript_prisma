import { CreateEquipamentsModelDto } from './create-equipaments-model.req.dto';
import EquipamentsModelIncludeDataQuery from './equipament-includedata-query.req.dto';
import EquipamentsModelPaginationQuery from './equipament-pagination-query.req.dto';
import { UpdateEquipamentsModelDto } from './update-equipaments-model.req.dto';

export {
  CreateEquipamentsModelDto,
  UpdateEquipamentsModelDto,
  EquipamentsModelPaginationQuery,
  EquipamentsModelIncludeDataQuery,
};
