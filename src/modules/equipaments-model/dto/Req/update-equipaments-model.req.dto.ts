import { PartialType } from '@nestjs/swagger';
import { CreateEquipamentsModelDto } from './create-equipaments-model.req.dto';

export class UpdateEquipamentsModelDto extends PartialType(
  CreateEquipamentsModelDto,
) {}
