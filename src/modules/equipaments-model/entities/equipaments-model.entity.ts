import { ApiProperty } from '@nestjs/swagger';
import { EquipamentsModel } from '@prisma/client';
import { IsDate, IsNumber, IsString } from 'class-validator';

export class EquipamentsModelEntity implements EquipamentsModel {
  @ApiProperty({
    type: 'number',
    description: 'ID auto incrementado no banco de dados',
  })
  @IsNumber(
    {},
    { message: 'This Number is auto generate and auto increment in database' },
  )
  id: number;

  @ApiProperty({ type: 'string', description: 'name of the equipamentModel' })
  @IsString({ message: 'O atributo name deve ser uma string' })
  name: string;

  @ApiProperty({ type: 'number', description: 'ID of the category' })
  @IsNumber({}, { message: 'O categoryId deve ser um número' })
  categoryId: number;

  @ApiProperty({ type: 'number', description: 'ID of the family' })
  @IsNumber({}, { message: 'O familyId deve ser um número' })
  familyId: number;

  @ApiProperty({ type: 'number', description: 'ID of the subfamily' })
  @IsNumber({}, { message: 'O subfamilyId deve ser um número' })
  sub_FamilyId: number;

  @ApiProperty({ type: 'number', description: 'ID of the fabricator' })
  @IsNumber({}, { message: 'O fabricatorId deve ser um número' })
  fabricatorId: number;

  @ApiProperty({ type: 'date', description: 'date is genarated in database' })
  @IsDate({ message: 'O atributo CreatedAt não esta presente' })
  createdAt: Date;

  @ApiProperty({ type: 'date', description: 'date is genarated in database' })
  @IsDate({ message: 'O atributo UpdatedAt não esta presente' })
  updatedAt: Date;
}
