import { Test, TestingModule } from '@nestjs/testing';
import { EquipamentsModelController } from './equipaments-model.controller';
import { EquipamentsModelService } from './equipaments-model.service';

describe('EquipamentsModelController', () => {
  let controller: EquipamentsModelController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EquipamentsModelController],
      providers: [EquipamentsModelService],
    }).compile();

    controller = module.get<EquipamentsModelController>(
      EquipamentsModelController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
