import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { EquipamentsModelService } from './equipaments-model.service';
import { CreateEquipamentsModelDto } from './dto/Req/create-equipaments-model.req.dto';
import { UpdateEquipamentsModelDto } from './dto/Req/update-equipaments-model.req.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import EquipamentsModelPaginationQuery from './dto/Req/equipament-pagination-query.req.dto';
import { EquipamentsModelResDto } from './dto/Res';
import { EquipamentsModelIncludeDataQuery } from './dto/Req';

@ApiBearerAuth()
@ApiTags('EquipamentsModels')
@Controller('equipaments-model')
export class EquipamentsModelController {
  constructor(
    private readonly equipamentsModelService: EquipamentsModelService,
  ) {}

  @Post()
  create(
    @Body() createEquipamentsModelDto: CreateEquipamentsModelDto,
  ): Promise<EquipamentsModelResDto> {
    return this.equipamentsModelService.create(createEquipamentsModelDto);
  }

  @Get()
  findAll(
    @Query() params: EquipamentsModelPaginationQuery,
  ): Promise<EquipamentsModelResDto> {
    return this.equipamentsModelService.findAll(params);
  }

  @Get(':id')
  findOne(
    @Param('id') id: string,
    @Query() params: EquipamentsModelIncludeDataQuery,
  ): Promise<EquipamentsModelResDto> {
    return this.equipamentsModelService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateEquipamentsModelDto: UpdateEquipamentsModelDto,
  ): Promise<EquipamentsModelResDto> {
    return this.equipamentsModelService.update(+id, updateEquipamentsModelDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<EquipamentsModelResDto> {
    return this.equipamentsModelService.remove(+id);
  }
}
