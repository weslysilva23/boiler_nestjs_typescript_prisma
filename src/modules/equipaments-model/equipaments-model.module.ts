import { Module } from '@nestjs/common';
import { EquipamentsModelService } from './equipaments-model.service';
import { EquipamentsModelController } from './equipaments-model.controller';
import { EquipamentsModelRepository } from './repositories/equipaments-model.repository';
import { PrismaService } from '@services/prisma/prisma.service';

@Module({
  controllers: [EquipamentsModelController],
  providers: [
    EquipamentsModelService,
    PrismaService,
    EquipamentsModelRepository,
  ],
})
export class EquipamentsModelModule {}
