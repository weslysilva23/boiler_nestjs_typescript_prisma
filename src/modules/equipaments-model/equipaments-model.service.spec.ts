import { Test, TestingModule } from '@nestjs/testing';
import { EquipamentsModelService } from './equipaments-model.service';

describe('EquipamentsModelService', () => {
  let service: EquipamentsModelService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EquipamentsModelService],
    }).compile();

    service = module.get<EquipamentsModelService>(EquipamentsModelService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
