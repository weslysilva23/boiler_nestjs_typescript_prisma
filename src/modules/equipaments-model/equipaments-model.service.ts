import { Injectable } from '@nestjs/common';
import { CreateEquipamentsModelDto } from './dto/Req/create-equipaments-model.req.dto';
import { UpdateEquipamentsModelDto } from './dto/Req/update-equipaments-model.req.dto';
import { EquipamentsModel } from '@prisma/client';
import { EquipamentsModelRepository } from './repositories/equipaments-model.repository';
import { NotFoundError } from '@common/types';
import EquipamentsModelPaginationQuery from './dto/Req/equipament-pagination-query.req.dto';
import { EquipamentsModelResDto } from './dto/Res/equipaments-model.res.dto';

@Injectable()
export class EquipamentsModelService {
  constructor(
    private readonly equipamentsModelRepo: EquipamentsModelRepository,
  ) {}

  async create(
    createEquipamentDto: CreateEquipamentsModelDto,
  ): Promise<EquipamentsModelResDto> {
    //TODO verificar se o equipamento ja existe se existe apenas atualizar as informações.
    //TODO Senao criar

    try {
      const equipament =
        await this.equipamentsModelRepo.create(createEquipamentDto);
      return equipament;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    query: EquipamentsModelPaginationQuery,
  ): Promise<EquipamentsModelResDto> {
    try {
      return this.equipamentsModelRepo.findAll(query);
    } catch (error) {
      console.log(`Error to try get all equipaments`, error);
      throw error;
    }
  }

  async findOne(
    id: number,
    query?: EquipamentsModelPaginationQuery,
  ): Promise<EquipamentsModelResDto> {
    //TODO Caso equipamento nao seja encontrado retornar um Not Found - throw new NotFoundError

    let equipament = await this.equipamentsModelRepo.findOne(id, query);

    if (equipament) {
      return equipament;
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  async update(
    id: number,
    updateEquipamentDto: UpdateEquipamentsModelDto,
  ): Promise<EquipamentsModelResDto> {
    try {
      return await this.equipamentsModelRepo.update(id, updateEquipamentDto);
    } catch (error) {
      throw error;
    }
  }

  async remove(
    id: number,
    query?: EquipamentsModelPaginationQuery,
  ): Promise<EquipamentsModelResDto> {
    return await this.equipamentsModelRepo.remove(id);
  }
}
