import { Injectable } from '@nestjs/common';
import { PrismaService } from '@services/prisma/prisma.service';
import {
  CreateEquipamentsModelDto,
  UpdateEquipamentsModelDto,
} from '../dto/Req';
import { NotFoundError } from '@common/types/http.types';
import EquipamentsModelPaginationQuery from '../dto/Req/equipament-pagination-query.req.dto';
import { EOrder } from '@common/enum';
import { EquipamentsModelResDto } from '../dto/Res/equipaments-model.res.dto';

@Injectable()
export class EquipamentsModelRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(
    createEquipamentDto: CreateEquipamentsModelDto,
  ): Promise<EquipamentsModelResDto> {
    const created = await this.prisma.equipamentsModel.create({
      data: createEquipamentDto,
    });

    return {
      statusCode: 200,
      data: created,
      status: 'success',
    };
  }

  async findAll(
    query: EquipamentsModelPaginationQuery,
  ): Promise<EquipamentsModelResDto> {
    const {
      pageNumber = 1,
      pageSize = 10,
      order = EOrder.Ascending,
      orderBy = 'name',
      includeData = false,
    } = query;

    try {
      const orderByBuilded = this.orderByBuiler(orderBy, order);
      const totalElements = await this.prisma.equipamentsModel.count();
      const totalPages = Math.ceil(totalElements / pageSize);
      const skip = (pageNumber - 1) * pageSize;

      const equipamentsModelData = await this.prisma.equipamentsModel.findMany({
        orderBy: orderByBuilded,
        include: includeData
          ? {
              family: true,
              category: true,
              fabricator: true,
              subFamily: true,
            }
          : {},
        skip: skip,
        take: pageSize,
      });

      return {
        statusCode: 200,
        data: equipamentsModelData,
        status: 'success',
        meta: {
          pagination: {
            pageNumber,
            pageSize,
            totalElements,
            totalPages,
          },
        },
      };
    } catch (error) {
      console.log(`Error to try get all equipaments`, error);
      throw error;
    }
  }

  async findOne(
    id: number,
    query?: EquipamentsModelPaginationQuery,
  ): Promise<EquipamentsModelResDto> {
    const { includeData = false } = query;

    const equipament = await this.prisma.equipamentsModel.findUnique({
      where: {
        id,
      },
      include: includeData
        ? {
            family: true,
            category: true,
            fabricator: true,
            subFamily: true,
          }
        : {},
    });

    if (equipament) {
      return {
        statusCode: 200,
        data: equipament,
        status: 'success',
      };
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  async remove(id: number): Promise<EquipamentsModelResDto> {
    let deleted = await this.prisma.equipamentsModel.delete({
      where: {
        id,
      },
      include: {
        family: true,
        category: true,
        fabricator: true,
        subFamily: true,
      },
    });

    if (deleted) {
      return {
        statusCode: 200,
        data: deleted,
        status: 'success',
      };
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  async update(
    id: number,
    updateEquipamentDto: UpdateEquipamentsModelDto,
  ): Promise<EquipamentsModelResDto> {
    let updated = await this.prisma.equipamentsModel.update({
      data: updateEquipamentDto,
      where: {
        id,
      },
      include: {
        family: true,
        category: true,
        fabricator: true,
        subFamily: true,
      },
    });

    if (updated) {
      return {
        statusCode: 200,
        data: updated,
        status: 'success',
      };
    } else {
      throw new NotFoundError('Equipament Not Found');
    }
  }

  private orderByBuiler(orderBy: string, order: EOrder) {
    let orderBuilded = {};

    if (orderBy.includes('.')) {
      let orderSplited = order.split('.');
      let property = orderSplited[0];
      let subpropery = orderSplited[1];

      orderBuilded[property] = {
        [subpropery]: order.toLowerCase(),
      };
    } else {
      orderBuilded[orderBy] = order.toLowerCase();
    }

    return orderBuilded;
  }
}
