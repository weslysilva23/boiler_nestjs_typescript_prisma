import { ApiProperty } from '@nestjs/swagger';
import { Family } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsDate, IsString } from 'class-validator';

export class CreateFamilyDto implements Omit<Family, 'id' | 'createdAt'> {
  @ApiProperty({ type: 'string', description: 'Name of the Family' })
  @IsString()
  name: string;

  @ApiProperty({
    type: 'string',
    description: 'Date the Family was last updated',
  })
  @Transform(param => new Date())
  @IsDate()
  updatedAt: Date = new Date();
}
