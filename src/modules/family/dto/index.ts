import { CreateFamilyDto } from './create-family.dto';
import { UpdateFamilyDto } from './update-family.dto';

export { CreateFamilyDto, UpdateFamilyDto };
