import { ApiProperty } from '@nestjs/swagger';
import { Family } from '@prisma/client';

export class FamilyEntity implements Family {
  @ApiProperty({ type: 'string', description: 'Date the Family was created' })
  createdAt: Date;
  @ApiProperty({ type: 'number', description: 'ID of the Family' })
  id: number;

  @ApiProperty({ type: 'string', description: 'Name of the Family' })
  name: string;
  @ApiProperty({
    type: 'string',
    description: 'Date the Family was last updated',
  })
  updatedAt: Date;
}
