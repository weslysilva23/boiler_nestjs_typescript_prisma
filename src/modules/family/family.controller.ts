import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FamilyService } from './family.service';
import { CreateFamilyDto } from './dto/create-family.dto';
import { UpdateFamilyDto } from './dto/update-family.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('Family')
@Controller('family')
export class FamilyController {
  constructor(private readonly familyService: FamilyService) {}

  @Post()
  private create(@Body() createFamilyDto: CreateFamilyDto) {
    return this.familyService.create(createFamilyDto);
  }

  @Get()
  private findAll() {
    return this.familyService.findAll();
  }

  @Get(':id')
  private findOne(@Param('id') id: string) {
    return this.familyService.findOne(+id);
  }

  @Delete(':id')
  private remove(@Param('id') id: string) {
    return this.familyService.remove(+id);
  }

  @Patch(':id')
  private update(
    @Param('id') id: string,
    @Body() updateFamilyDto: UpdateFamilyDto,
  ) {
    return this.familyService.update(+id, updateFamilyDto);
  }
}
