import { Module } from '@nestjs/common';
import { FamilyService } from './family.service';
import { FamilyController } from './family.controller';
import { PrismaService } from '@services/prisma/prisma.service';
import { FamilyRepository } from './repositories/family.repository';

@Module({
  controllers: [FamilyController],
  providers: [FamilyService, PrismaService, FamilyRepository],
})
export class FamilyModule {}
