import { Injectable } from '@nestjs/common';
import { CreateFamilyDto } from './dto/create-family.dto';
import { UpdateFamilyDto } from './dto/update-family.dto';
import { FamilyRepository } from './repositories/family.repository';
import { NotFoundError } from '@common/types';

@Injectable()
export class FamilyService {
  constructor(private readonly familyRepo: FamilyRepository) {}

  async create(createFamilyDto: CreateFamilyDto) {
    try {
      const familyCreated = await this.familyRepo.create(createFamilyDto);
      return familyCreated;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const categories = await this.familyRepo.findAll();
      return categories;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number) {
    let family = await this.familyRepo.findOne(id);

    if (family) {
      return family;
    } else {
      throw new NotFoundError('Family Not Found');
    }
  }

  async remove(id: number) {
    return await this.familyRepo.remove(id);
  }

  async update(id: number, updateFamilyDto: UpdateFamilyDto) {
    try {
      return await this.familyRepo.update(id, updateFamilyDto);
    } catch (error) {
      throw error;
    }
  }
}
