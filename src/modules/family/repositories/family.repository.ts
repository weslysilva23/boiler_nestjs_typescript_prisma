import { Injectable } from '@nestjs/common';
import { PrismaService } from '@services/prisma/prisma.service';
import { CreateFamilyDto, UpdateFamilyDto } from '../dto';
import { FamilyEntity } from '../entities/family.entity';
import { NotFoundError } from '@common/types/http.types';

@Injectable()
export class FamilyRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(createFamilyDto: CreateFamilyDto): Promise<FamilyEntity> {
    return this.prisma.family.create({
      data: createFamilyDto,
    });
  }

  async findAll(): Promise<FamilyEntity[]> {
    try {
      return this.prisma.family.findMany();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<FamilyEntity> {
    //TODO Caso Familyo nao seja encontrado retornar um Not Found - throw new NotFoundError

    let Family = await this.prisma.family.findUnique({
      where: {
        id,
      },
    });

    if (Family) {
      return Family;
    } else {
      throw new NotFoundError('Family Not Found');
    }
  }

  async remove(id: number): Promise<FamilyEntity> {
    let deleted = await this.prisma.family.delete({
      where: {
        id,
      },
    });

    return deleted;
  }

  async update(
    id: number,
    updateFamilyDto: UpdateFamilyDto,
  ): Promise<FamilyEntity> {
    let updated = await this.prisma.family.update({
      data: updateFamilyDto,
      where: {
        id,
      },
    });

    return updated;
  }
}
