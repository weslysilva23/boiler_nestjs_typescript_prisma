import { Controller, Get } from '@nestjs/common';
import { HealthService } from './health.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('Health')
@Controller('/health')
export class HealthController {
  constructor(private readonly healthService: HealthService) {}

  // @ApiBearerAuth()
  // @UseGuards()
  @Get()
  getHello(): string {
    return this.healthService.getHello();
  }
}
