import { ApiProperty } from '@nestjs/swagger';
import { Sub_Family } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsString, IsDate } from 'class-validator';

export class CreateSubFamilyDto
  implements Omit<Sub_Family, 'id' | 'createdAt'>
{
  @ApiProperty({ type: 'string', description: 'Name of the Family' })
  @IsString()
  name: string;

  @ApiProperty({
    type: 'string',
    description: 'Date the Family was last updated',
  })
  @Transform(param => new Date())
  @IsDate()
  updatedAt: Date = new Date();
}
