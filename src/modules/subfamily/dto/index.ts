import { CreateSubFamilyDto } from './create-subfamily.dto';
import { UpdateSubFamilyDto } from './update-subfamily.dto';

export { CreateSubFamilyDto, UpdateSubFamilyDto };
