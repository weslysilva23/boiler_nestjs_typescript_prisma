import { PartialType } from '@nestjs/swagger';
import { CreateSubFamilyDto } from './create-subfamily.dto';

export class UpdateSubFamilyDto extends PartialType(CreateSubFamilyDto) {}
