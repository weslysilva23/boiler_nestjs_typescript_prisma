import { ApiProperty } from '@nestjs/swagger';
import { Sub_Family } from '@prisma/client';

export class SubFamilyEntity implements Sub_Family {
  @ApiProperty({
    type: 'string',
    description: 'Date the SubFamily was created',
  })
  createdAt: Date;
  @ApiProperty({ type: 'number', description: 'ID of the SubFamily' })
  id: number;

  @ApiProperty({ type: 'string', description: 'Name of the SubFamily' })
  name: string;
  @ApiProperty({
    type: 'string',
    description: 'Date the SubFamily was last updated',
  })
  updatedAt: Date;
}
