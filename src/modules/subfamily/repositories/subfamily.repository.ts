import { Injectable } from '@nestjs/common';
import { PrismaService } from '@services/prisma/prisma.service';
import { CreateSubFamilyDto, UpdateSubFamilyDto } from '../dto';
import { SubFamilyEntity } from '../entities/subfamily.entity';
import { NotFoundError } from '@common/types/http.types';

@Injectable()
export class SubFamilyRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(
    createSubFamilyDto: CreateSubFamilyDto,
  ): Promise<SubFamilyEntity> {
    return this.prisma.family.create({
      data: createSubFamilyDto,
    });
  }

  async findAll(): Promise<SubFamilyEntity[]> {
    try {
      return this.prisma.family.findMany();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<SubFamilyEntity> {
    //TODO Caso SubFamilyo nao seja encontrado retornar um Not Found - throw new NotFoundError

    let SubFamily = await this.prisma.family.findUnique({
      where: {
        id,
      },
    });

    if (SubFamily) {
      return SubFamily;
    } else {
      throw new NotFoundError('SubFamily Not Found');
    }
  }

  async remove(id: number): Promise<SubFamilyEntity> {
    let deleted = await this.prisma.family.delete({
      where: {
        id,
      },
    });

    return deleted;
  }

  async update(
    id: number,
    updateSubFamilyDto: UpdateSubFamilyDto,
  ): Promise<SubFamilyEntity> {
    let updated = await this.prisma.family.update({
      data: updateSubFamilyDto,
      where: {
        id,
      },
    });

    return updated;
  }
}
