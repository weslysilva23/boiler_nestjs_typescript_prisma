import { Test, TestingModule } from '@nestjs/testing';
import { SubfamilyController } from './subfamily.controller';
import { SubfamilyService } from './subfamily.service';

describe('SubfamilyController', () => {
  let controller: SubfamilyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubfamilyController],
      providers: [SubfamilyService],
    }).compile();

    controller = module.get<SubfamilyController>(SubfamilyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
