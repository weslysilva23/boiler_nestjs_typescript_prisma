import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { SubFamilyService } from './subfamily.service';
import { CreateSubFamilyDto } from './dto/create-subfamily.dto';
import { UpdateSubFamilyDto } from './dto/update-subfamily.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('Subfamily')
@Controller('subfamily')
export class SubFamilyController {
  constructor(private readonly subfamilyService: SubFamilyService) {}

  @Post()
  private create(@Body() createSubFamilyDto: CreateSubFamilyDto) {
    return this.subfamilyService.create(createSubFamilyDto);
  }

  @Get()
  private findAll() {
    return this.subfamilyService.findAll();
  }

  @Get(':id')
  private findOne(@Param('id') id: string) {
    return this.subfamilyService.findOne(+id);
  }

  @Delete(':id')
  private remove(@Param('id') id: string) {
    return this.subfamilyService.remove(+id);
  }

  @Patch(':id')
  private update(
    @Param('id') id: string,
    @Body() updateSubFamilyDto: UpdateSubFamilyDto,
  ) {
    return this.subfamilyService.update(+id, updateSubFamilyDto);
  }
}
