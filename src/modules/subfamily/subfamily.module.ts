import { Module } from '@nestjs/common';
import { SubFamilyService } from './subfamily.service';
import { SubFamilyController } from './subfamily.controller';
import { PrismaService } from '@services/prisma/prisma.service';
import { SubFamilyRepository } from './repositories/subfamily.repository';

@Module({
  controllers: [SubFamilyController],
  providers: [SubFamilyService, PrismaService, SubFamilyRepository],
})
export class SubfamilyModule {}
