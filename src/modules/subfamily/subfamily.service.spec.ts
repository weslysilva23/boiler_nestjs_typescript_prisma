import { Test, TestingModule } from '@nestjs/testing';
import { SubfamilyService } from './subfamily.service';

describe('SubfamilyService', () => {
  let service: SubfamilyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubfamilyService],
    }).compile();

    service = module.get<SubfamilyService>(SubfamilyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
