import { Injectable } from '@nestjs/common';
import { CreateSubFamilyDto, UpdateSubFamilyDto } from './dto';
import { SubFamilyRepository } from './repositories/subfamily.repository';
import { NotFoundError } from '@common/types';

@Injectable()
export class SubFamilyService {
  constructor(private readonly familyRepo: SubFamilyRepository) {}

  async create(createSubFamilyDto: CreateSubFamilyDto) {
    try {
      const familyCreated = await this.familyRepo.create(createSubFamilyDto);
      return familyCreated;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const categories = await this.familyRepo.findAll();
      return categories;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number) {
    let family = await this.familyRepo.findOne(id);

    if (family) {
      return family;
    } else {
      throw new NotFoundError('SubFamily Not Found');
    }
  }

  async remove(id: number) {
    return await this.familyRepo.remove(id);
  }

  async update(id: number, updateSubFamilyDto: UpdateSubFamilyDto) {
    try {
      return await this.familyRepo.update(id, updateSubFamilyDto);
    } catch (error) {
      throw error;
    }
  }
}
