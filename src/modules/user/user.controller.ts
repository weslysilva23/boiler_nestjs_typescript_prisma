import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RoleGuard } from '@shared/middlewares/guard/role.guard';
import { EquipamentResDto } from '@modules/equipament/dto';

@ApiBearerAuth()
@ApiTags('Usuario')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @Post()
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  private create(@Body() createUserDto: CreateUserDto): any {
    return this.userService.create(createUserDto);
  }

  @ApiBearerAuth()
  @Get()
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  private findAll() {
    return this.userService.findAll();
  }

  @ApiBearerAuth()
  @Get(':id')
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  private findOne(@Param('id') id: string) {
    return this.userService.findOne(+id);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  private remove(@Param('id') id: string) {
    return this.userService.remove(+id);
  }

  @ApiBearerAuth()
  @Patch(':id')
  @UseGuards(new RoleGuard(['admin', 'supervisor']))
  private update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.userService.update(+id, updateUserDto);
  }
}
