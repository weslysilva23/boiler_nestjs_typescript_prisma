import { IsObject, IsOptional, ValidateNested } from 'class-validator';
import Pagination from './pagination';
import { Type } from 'class-transformer';
import { IMeta } from '@common/interfaces/http';
import { ApiProperty } from '@nestjs/swagger';

export default class Meta implements IMeta {
  @ApiProperty()
  @ValidateNested({ each: true })
  @Type(() => Pagination)
  @IsOptional()
  pagination?: Pagination;
}
