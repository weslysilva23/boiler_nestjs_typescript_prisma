import { Transform } from 'class-transformer';
import { IsNumber } from 'class-validator';
import { IPagination } from '@common/interfaces/http';
import { ApiProperty } from '@nestjs/swagger';

export default class Pagination implements IPagination {
  @ApiProperty()
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  pageNumber: number;
  @ApiProperty()
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  pageSize: number;
  @ApiProperty()
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  totalElements: number;
  @ApiProperty()
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  totalPages: number;
}
