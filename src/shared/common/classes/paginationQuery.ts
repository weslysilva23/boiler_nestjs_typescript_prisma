import {
  IsEnum,
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  Max,
} from 'class-validator';
import { EOrder } from '../enum/order.enum';
import { Transform } from 'class-transformer';
import { IPaginationQuery } from '@common/interfaces/http';

export default class PaginationQuery {
  @IsEnum(EOrder)
  @IsOptional()
  private order?: EOrder;
  @IsOptional()
  @IsString()
  private orderBy?: string;
  @Transform(param => Number.parseInt(param.value))
  @IsOptional()
  @IsInt()
  @IsNumber()
  private pageNumber?: number;
  @Transform(param => Number.parseInt(param.value))
  @IsNumber()
  @IsInt()
  @Max(250)
  @IsOptional()
  private pageSize?: number;
}
