// import { UnauthorizedException } from "@nestjs/common";

// export const AuthRole = (role: string) => {
//     return (target, key, descriptor) => {
//       const originalMethod = descriptor.value;
//       descriptor.value = function (...args) {
//         const user = args[0];
//         if (user.role !== role) {
//           throw new UnauthorizedException;
//         }
//         return originalMethod.apply(this, args);
//       };
//       return descriptor;
//     };
//   };

import { UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';

export const AuthRole = (role: string) => {
  return (target, key, descriptor) => {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
      const req: Request = args[0]; // Primeiro argumento é a requisição
      const user = req['user']; // Assumindo que o usuário esteja disponível em req.user
      if (!user || user.role !== role) {
        throw new UnauthorizedException();
      }
      return originalMethod.apply(this, args);
    };
    return descriptor;
  };
};
