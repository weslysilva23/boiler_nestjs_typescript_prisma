export enum EUserRole {
  ADMIN = 'admin',
  TECNICO = 'tecnico',
  FINANCEIRO = 'financeiro',
}
