import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  UnauthorizedException,
  NotImplementedException,
  NotFoundException,
  BadRequestException,
  BadGatewayException,
  PreconditionFailedException,
  ConflictException,
  NotAcceptableException,
  HttpVersionNotSupportedException,
  GatewayTimeoutException,
  UnsupportedMediaTypeException,
  ServiceUnavailableException,
  ForbiddenException,
  PayloadTooLargeException,
  MethodNotAllowedException,
  HttpException,
} from '@nestjs/common';

import {
  BadGatewayError,
  BadRequestError,
  NotFoundError,
  NotImplementedError,
  UnauthorizedError,
  PreconditionFailedError,
  ConflictError,
  NotAcceptableError,
  HttpVersionNotSupportedError,
  GatewayTimeoutError,
  UnsupportedMediaTypeError,
  ServiceUnavailableError,
  ForbiddenError,
  PayloadTooLargeError,
  MethodNotAllowedError,
  NotModifiedError,
  GetwayTimeoutError,
  NoContentError,
} from '../types/http.types';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpStatus } from '@nestjs/common/enums';

@Injectable()
export class HttpInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError(error => {
        if (error instanceof UnauthorizedError)
          throw new UnauthorizedException(error.message);
        if (error instanceof NotImplementedError)
          throw new NotImplementedException(error.message);
        if (error instanceof NotFoundError)
          throw new NotFoundException(error.message);
        if (error instanceof BadGatewayError)
          throw new BadGatewayException(error.message);
        if (error instanceof BadRequestError)
          throw new BadRequestException(error.message);
        if (error instanceof PreconditionFailedError)
          throw new PreconditionFailedException(error.message);
        if (error instanceof ConflictError)
          throw new ConflictException(error.message);
        if (error instanceof NotAcceptableError)
          throw new NotAcceptableException(error.message);
        if (error instanceof HttpVersionNotSupportedError)
          throw new HttpVersionNotSupportedException(error.message);
        if (error instanceof GatewayTimeoutError)
          throw new GatewayTimeoutException(error.message);
        if (error instanceof UnsupportedMediaTypeError)
          throw new UnsupportedMediaTypeException(error.message);
        if (error instanceof ServiceUnavailableError)
          throw new ServiceUnavailableException(error.message);
        if (error instanceof ForbiddenError)
          throw new ForbiddenException(error.message);
        if (error instanceof PayloadTooLargeError)
          throw new PayloadTooLargeException(error.message);
        if (error instanceof MethodNotAllowedError)
          throw new MethodNotAllowedException(error.message);
        if (error instanceof NotModifiedError)
          throw new HttpException(error.message, HttpStatus.NOT_MODIFIED); //Adapted
        if (error instanceof GetwayTimeoutError)
          throw new HttpException(error.message, HttpStatus.GATEWAY_TIMEOUT); //Adapted
        if (error instanceof NoContentError)
          throw new HttpException(error.message, HttpStatus.NO_CONTENT); //Adapted

        throw error;
      }),
    );
  }
}
