import { PrismaUtil } from '@common/utils';
import { handleDatabaseError } from '@common/utils/handle-database-error.utils';
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PrismaInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError(error => {
        if (PrismaUtil.isPrismaError(error)) {
          error = handleDatabaseError(error);
        }
        throw error;
      }),
    );
  }
}
