import IPagination from './ipagination';
import IMeta from './meta.interface';
import IPaginationQuery from './paginationQuery.interface';
import IResponse from './response.interface';

export { IPagination, IMeta, IPaginationQuery, IResponse };
