export default interface IPagination {
  totalPages: number;
  totalElements: number;
  pageNumber: number;
  pageSize: number;
}
