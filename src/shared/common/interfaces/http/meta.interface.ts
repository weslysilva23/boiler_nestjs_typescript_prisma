import IPagination from './ipagination';

export default interface IMeta {
  pagination?: IPagination;
}
