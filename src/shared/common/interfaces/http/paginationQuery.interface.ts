import { EOrder } from '../../enum/order.enum';

export default interface IPaginationQuery {
  pageSize?: number;
  pageNumber?: number;
  order?: EOrder;
  orderBy?: string;
  includeData?: boolean;
}
