import IMeta from './meta.interface';

export default interface IResponse<T> {
  statusCode: number;
  status: string;
  data: T | T[];
  meta?: IMeta;
  summary?: any;
}
