import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const AUTH_SECRET: string = process.env.AUTH_SECRET;
    if (!AUTH_SECRET) {
      console.error('Segredo não definido');
      throw new UnauthorizedException(
        'Configuração inválida: Segredo não definido',
      );
    }
    const token = req.headers.authorization?.split(' ')[1];

    if (!token) {
      console.error('Token não fornecido');
      throw new UnauthorizedException('Token não fornecido');
    }

    try {
      const decoded = jwt.verify(token, AUTH_SECRET);
      req['user'] = decoded;
      next();
    } catch (error) {
      console.error('Token inválido:', error.message);
      throw new UnauthorizedException('Token inválido');
    }
  }
}
