import { Request, Response, NextFunction } from 'express';

interface TExtendedRequest extends Request {
  user?: any;
  roles?: string[];
}

interface TExtendedResponse extends Response {}

interface TExtendedNextFunction extends NextFunction {}

export type { TExtendedRequest, TExtendedResponse, TExtendedNextFunction };
