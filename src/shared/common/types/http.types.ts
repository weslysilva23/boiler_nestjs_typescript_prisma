class UnauthorizedError extends Error {}
class NotFoundError extends Error {}
class NotImplementedError extends Error {}
class BadGatewayError extends Error {}
class BadRequestError extends Error {}
class PreconditionFailedError extends Error {}
class ConflictError extends Error {}
class NotAcceptableError extends Error {}
class HttpVersionNotSupportedError extends Error {}
class GatewayTimeoutError extends Error {}
class UnsupportedMediaTypeError extends Error {}
class ServiceUnavailableError extends Error {}
class ForbiddenError extends Error {}
class PayloadTooLargeError extends Error {}
class MethodNotAllowedError extends Error {}
class NotModifiedError extends Error {}
class GetwayTimeoutError extends Error {}
class NoContentError extends Error {}

export {
  UnauthorizedError,
  NotFoundError,
  NotImplementedError,
  BadGatewayError,
  BadRequestError,
  PreconditionFailedError,
  ConflictError,
  NotAcceptableError,
  HttpVersionNotSupportedError,
  GatewayTimeoutError,
  UnsupportedMediaTypeError,
  ServiceUnavailableError,
  ForbiddenError,
  PayloadTooLargeError,
  MethodNotAllowedError,
  NotModifiedError,
  GetwayTimeoutError,
  NoContentError,
};
