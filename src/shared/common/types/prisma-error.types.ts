import { Prisma } from '@prisma/client';
import {
  BadGatewayError,
  BadRequestError,
  ConflictError,
  PreconditionFailedError,
} from './http.types';

export type PrismaClientError = Prisma.PrismaClientKnownRequestError & {
  name: string;
  code: string;
  clientVersion: string;
  meta?: {
    modelName: string;
    field_name: string;
    target: string;
  };
};

export class AuthenticationError extends BadGatewayError {
  constructor(e: PrismaClientError) {
    const server = e.meta.target;
    super(`Authentication failed against database on server  ${server}`);
  }
}

export class UniqueConstraintError extends ConflictError {
  constructor(e: PrismaClientError) {
    const uniqueField = e.meta.target;

    super(`A record with this ${uniqueField} already exist`);
  }
}

export class ForeignKeyConstraintError extends PreconditionFailedError {
  constructor(e: PrismaClientError) {
    super(`Foreign key constraint failed on the field: 
    
    ${JSON.parse(JSON.stringify(e))}`);
  }
}

export class DataValidationError extends BadRequestError {
  constructor(e: PrismaClientError) {
    super(`Data validation error: ${e.message}`);
  }
}

export class QueryValidationError extends BadRequestError {
  constructor(e: PrismaClientError) {
    super(`Data validation error: ${e.message}`);
  }
}
