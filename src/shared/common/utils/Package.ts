import * as fs from 'fs';

export default class Package {
  private static packageData: any;

  constructor() {
    try {
      const packageJson = fs.readFileSync('package.json', 'utf-8');
      Package.packageData = JSON.parse(packageJson);
    } catch (error) {
      console.error('Erro ao ler o arquivo package.json:', error);
    }
  }

  public getVersion(): string {
    if (Package.packageData && Package.packageData.version) {
      return Package.packageData.version;
    } else {
      console.error('Erro ao obter a versão do pacote.');
      return 'Erro ao obter a versão';
    }
  }

  public getAuthor(): string {
    if (Package.packageData && Package.packageData.author) {
      return Package.packageData.author;
    } else {
      console.error('Erro ao obter a autor do pacote.');
      return 'Erro ao obter a autor';
    }
  }
}
