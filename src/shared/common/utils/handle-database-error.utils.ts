import { DatabaseError } from '@common/types';
import {
  AuthenticationError,
  DataValidationError,
  ForeignKeyConstraintError,
  PrismaClientError,
  QueryValidationError,
  UniqueConstraintError,
} from '@common/types/prisma-error.types';

enum EPrismaError {
  AuthenticationErrorFail = 'P1000',
  UniqueConstraintErrorFail = 'P2002',
  ForeignKeyConstraintErrorFail = 'P2003',
  DataValidationErrorFail = 'P2007',
  QueryValidationErrorFail = 'P2009',
}

export const handleDatabaseError = (e: PrismaClientError): Error => {
  switch (e.code) {
    case EPrismaError.AuthenticationErrorFail:
      return new AuthenticationError(e);
    case EPrismaError.UniqueConstraintErrorFail:
      return new UniqueConstraintError(e);
    case EPrismaError.ForeignKeyConstraintErrorFail:
      return new ForeignKeyConstraintError(e);
    case EPrismaError.DataValidationErrorFail:
      return new DataValidationError(e);
    case EPrismaError.QueryValidationErrorFail:
      return new QueryValidationError(e);

    default:
      new DatabaseError(e.message);
  }
};
