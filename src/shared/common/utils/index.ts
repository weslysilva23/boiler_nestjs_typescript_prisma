import { isPrismaError } from './is-prisma-error.util';
import Package from './Package';

const PrismaUtil = {
  isPrismaError,
};

export { PrismaUtil, Package };
